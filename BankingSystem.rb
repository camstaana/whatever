class Bank
	def initialize(first_name="Camila", last_name="Sta.Ana", balance=100)
		@first_name = first_name
		@last_name = last_name
		@balance = balance
	end 

	def full_name 
		puts "#{first_name} #{last_name}."
	end

	def new_full_name(new_fname="JC", new_lname="Rosales")
		@first_name = new_fname
		@last_name = new_lname
	end
	
	def display_balance
		puts "Balance: $#{@balance}."
	end
	
	def withdraw(amount)
		@balance -= amount
		puts "Withdrew #{amount}. New balance: $#{@balance}."
	end

	def deposit(amount)
		@balance += amount 
		puts "Deposited #{amount}. New balance: $#{@balance}."
	end

	def transaction_record
		if Bank.withdraw 
			puts "#{first_name} #{last_name} withdrew, and has a remaining balance of #{amount}."
end
